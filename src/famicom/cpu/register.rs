use super::*;

pub struct ByteLoaded<'a> {
    cpu: &'a mut CPU,
    byte: u8,
}

impl<'a> ByteLoaded<'a> {
    pub fn write_a(&mut self) -> &mut Self {
        self.cpu.write_a(self.byte);
        self
    }
    pub fn write_x(&mut self) -> &mut Self {
        self.cpu.write_x(self.byte);
        self
    }
    pub fn write_y(&mut self) -> &mut Self {
        self.cpu.write_y(self.byte);
        self
    }
    pub fn write_sp(&mut self) -> &mut Self {
        self.cpu.write_sp(self.byte);
        self
    }
    pub fn write_pcl(&mut self) -> &mut Self {
        self.cpu.pc &= 0b1111_1111_0000_0000;
        self.cpu.pc += self.byte as u16;
        self
    }
    pub fn dec_pc(&mut self) -> &mut Self {
        self.cpu.pc -= 1;
        self
    }
    fn write_mem_pointed_by_addr(&mut self, m: &mut Memory, addr: u16) -> &mut Self {
        m.write_byte(addr, self.byte);
        self
    }
    pub fn set_n_by_byte(&mut self) -> &mut Self {
        self.cpu.set_n_by_byte(self.byte);
        self
    }
    pub fn set_z_by_byte(&mut self) -> &mut Self {
        self.cpu.set_z_by_byte(self.byte);
        self
    }
    pub fn add_signed_byte_from_mem_pointed_by_pc(&mut self, m: &mut Memory) -> &mut Self {
        let mut res = self.byte + self.cpu.read_byte_from_mem_pointed_by_pc(m).get();
        if res < self.byte {
            res += 1;
        }
        self.byte = res;
        self
    }
    fn get(&self) -> u8 {
        self.byte
    }
    fn new(cpu: &'a mut CPU, byte: u8) -> Self {
        ByteLoaded { cpu, byte }
    }
}

pub struct WordLoaded<'a> {
    cpu: &'a mut CPU,
    word: u16,
}

impl<'a> WordLoaded<'a> {
    pub fn add_x_with_carry(mut self) -> Self {
        self.word = self.cpu.add_16_8_with_carry(self.word, self.cpu.x);
        self
    }
    pub fn write_pc(&mut self) -> &mut Self {
        self.cpu.write_pc(self.word);
        self
    }
    pub fn dec_pc(&mut self) -> &mut Self {
        self.cpu.pc -= 1;
        self
    }
    pub fn read_byte_from_mem_pointed_by_result(self, m: &mut Memory) -> ByteLoaded<'a> {
        ByteLoaded::new(self.cpu, m.read_byte(self.word))
    }
    fn new(cpu: &'a mut CPU, word: u16) -> Self {
        WordLoaded { cpu, word }
    }
}

pub struct AddrLoaded<'a> {
    cpu: &'a mut CPU,
    addr: u16,
}

impl<'a> AddrLoaded<'a> {
    pub fn write_mem_pointed_by_addr_from_a(&mut self, m: &mut Memory) -> &mut Self {
        self.cpu.read_a().write_mem_pointed_by_addr(m, self.addr);
        self
    }

    fn new(cpu: &'a mut CPU, addr: u16) -> Self {
        AddrLoaded { cpu, addr }
    }
}

impl<'a> CPU {
    pub fn read_a(&'a mut self) -> ByteLoaded<'a> {
        ByteLoaded::new(self, self.a)
    }
    pub fn read_x(&'a mut self) -> ByteLoaded<'a> {
        ByteLoaded::new(self, self.x)
    }
    pub fn read_y(&'a mut self) -> ByteLoaded<'a> {
        ByteLoaded::new(self, self.y)
    }
    pub fn read_pcl(&'a mut self) -> ByteLoaded<'a> {
        ByteLoaded::new(self, self.pc as u8)
    }
    pub fn inc_x(&'a mut self) -> ByteLoaded<'a> {
        self.x += 1;
        self.read_x()
    }
    pub fn dec_y(&'a mut self) -> ByteLoaded<'a> {
        self.y -= 1;
        self.read_y()
    }
    pub fn read_byte_from_mem_pointed_by_pc(&'a mut self, m: &mut Memory) -> ByteLoaded<'a> {
        ByteLoaded::new(self, m.read_byte(self.read_pc()))
    }
    pub fn read_word_from_mem_pointed_by_pc(&'a mut self, m: &mut Memory) -> WordLoaded<'a> {
        let word = m.read_word(self.read_pc(), self.inc_pc().read_pc());
        WordLoaded::new(self, word)
    }
    pub fn read_addr_from_mem_pointed_by_pc(&'a mut self, m: &mut Memory) -> AddrLoaded<'a> {
        let addr = m.read_word(self.read_pc(), self.inc_pc().read_pc());
        AddrLoaded::new(self, addr)
    }
}

impl CPU {
    const N: u8 = 0b1000_0000;
    const I: u8 = 0b0000_0100;
    const Z: u8 = 0b0000_0010;
    const C: u8 = 0b0000_0001;

    const NN: u8 = !Self::N;
    const NZ: u8 = !Self::Z;

    const BYTE_NEGATIVE: u8 = 0b1000;

    pub fn read_pc(&self) -> u16 {
        self.pc
    }
    fn read_p(&self) -> u8 {
        self.p
    }
    pub fn write_a(&mut self, byte: u8) -> &mut Self {
        self.a = byte;
        self
    }
    pub fn write_x(&mut self, byte: u8) -> &mut Self {
        self.x = byte;
        self
    }
    pub fn write_y(&mut self, byte: u8) -> &mut Self {
        self.y = byte;
        self
    }
    pub fn write_pc(&mut self, word: u16) -> &mut Self {
        self.pc = word;
        self
    }
    pub fn write_sp(&mut self, byte: u8) -> &mut Self {
        self.sp = byte;
        self
    }
    pub fn inc_pc(&mut self) -> &mut Self {
        self.pc += 1;
        self
    }
    pub fn set_n(&mut self) -> &mut Self {
        self.p |= Self::N;
        self
    }
    pub fn set_z(&mut self) -> &mut Self {
        self.p |= Self::Z;
        self
    }
    pub fn set_i(&mut self) -> &mut Self {
        self.p |= Self::I;
        self
    }
    pub fn set_c(&mut self) -> &mut Self {
        self.p |= Self::C;
        self
    }
    pub fn unset_n(&mut self) -> &mut Self {
        self.p &= Self::NN;
        self
    }
    pub fn unset_z(&mut self) -> &mut Self {
        self.p &= Self::NZ;
        self
    }
    pub fn set_n_by_byte(&mut self, byte: u8) -> &mut Self {
        if (byte & Self::BYTE_NEGATIVE) == Self::BYTE_NEGATIVE {
            self.set_n()
        } else {
            self.unset_n()
        }
    }
    pub fn set_z_by_byte(&mut self, byte: u8) -> &mut Self {
        if byte == 0 {
            self.set_z()
        } else {
            self.unset_z()
        }
    }
    pub fn z_or_else<F: FnMut(&mut Self) -> ()>(&mut self, mut f: F) {
        if (self.read_p() & Self::Z) == Self::Z {
            ()
        } else {
            f(self)
        }
    }
    pub fn read_start_addr(&self) -> u16 {
        self.start_addr
    }
    pub fn write_start_addr(&mut self, word: u16) {
        self.start_addr = word;
    }
    pub fn add_16_8_with_carry(&mut self, left: u16, right: u8) -> u16 {
        let res = left + right as u16;
        if res < left {
            self.set_c();
        }
        res
    }
}
