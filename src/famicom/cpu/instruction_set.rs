use super::*;

pub fn x78_sei(cpu: &mut CPU) {
    cpu.set_i();
}

pub fn xa2_ldx_imm(cpu: &mut CPU, m: &mut Memory) {
    cpu.inc_pc()
        .read_byte_from_mem_pointed_by_pc(m)
        .write_x()
        .set_n_by_byte()
        .set_z_by_byte();
}

pub fn x9a_txs(cpu: &mut CPU) {
    cpu.read_x().write_sp();
}

pub fn xa9_lda_imm(cpu: &mut CPU, m: &mut Memory) {
    cpu.inc_pc()
        .read_byte_from_mem_pointed_by_pc(m)
        .write_a()
        .set_n_by_byte()
        .set_z_by_byte();
}

pub fn xbd_lda_abs_x(cpu: &mut CPU, m: &mut Memory) {
    cpu.inc_pc()
        .read_word_from_mem_pointed_by_pc(m)
        .add_x_with_carry()
        .read_byte_from_mem_pointed_by_result(m)
        .write_a()
        .set_n_by_byte()
        .set_z_by_byte();
}

pub fn x8d_sta_abs(cpu: &mut CPU, m: &mut Memory) {
    cpu.inc_pc()
        .read_addr_from_mem_pointed_by_pc(m)
        .write_mem_pointed_by_addr_from_a(m);
}

pub fn xa0_ldy_imm(cpu: &mut CPU, m: &mut Memory) {
    cpu.inc_pc()
        .read_byte_from_mem_pointed_by_pc(m)
        .write_y()
        .set_n_by_byte()
        .set_z_by_byte();
}

pub fn xe8_inx(cpu: &mut CPU) {
    cpu.inc_x().set_n_by_byte().set_z_by_byte();
}

pub fn x88_dey(cpu: &mut CPU) {
    cpu.dec_y().set_n_by_byte().set_z_by_byte();
}

pub fn xd0_bne_rel(cpu: &mut CPU, m: &mut Memory) {
    cpu.inc_pc().z_or_else(|cpu| {
        cpu.read_pcl()
            .add_signed_byte_from_mem_pointed_by_pc(m)
            .write_pcl()
            .dec_pc();
    });
}

pub fn x4c_jmp_abs(cpu: &mut CPU, m: &mut Memory) {
    cpu.inc_pc()
        .read_word_from_mem_pointed_by_pc(m)
        .write_pc()
        .dec_pc();
}
