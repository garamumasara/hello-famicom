use super::{gen_word, ppu::*};

pub struct RomData<'a> {
    prg_rom: &'a [u8],
    chr_rom: &'a [u8],
}

impl<'a> RomData<'a> {
    pub fn new(prg_rom: &'a [u8], chr_rom: &'a [u8]) -> RomData<'a> {
        RomData { prg_rom, chr_rom }
    }
}

pub struct Memory<'a> {
    prg_rom: &'a [u8],
    pub ppu: PPU<'a>,
}

impl<'a> Memory<'a> {
    pub fn new(rom_data: RomData<'a>) -> Memory<'a> {
        Memory {
            prg_rom: rom_data.prg_rom,
            ppu: PPU::new(rom_data.chr_rom),
        }
    }
    pub fn read_byte(&mut self, address: u16) -> u8 {
        match (0x8000 <= address, address) {
            (true, _) => self.prg_rom[(address - 0x8000) as usize],
            (_, 0x2000..=0x2007) => self.ppu.read_ppu(address),
            _ => {
                self.undef(address);
                0
            }
        }
    }
    pub fn read_word(&mut self, low_addr: u16, high_addr: u16) -> u16 {
        let low = self.read_byte(low_addr);
        let high = self.read_byte(high_addr);
        gen_word(low, high)
    }
    pub fn write_byte(&mut self, address: u16, data: u8) {
        match address {
            0x2000..=0x2007 => self.ppu.write_ppu(address, data),
            _ => self.undef(address),
        }
    }
    fn undef(&self, addr: u16) {
        println!("accessed currently undefined memory space: 0x{:x}", addr);
    }
}
