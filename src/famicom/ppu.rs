pub struct PPU<'a> {
    ppuctrl: u8,
    // ppumask: u8,
    // ppustatus: u8,
    // oamaddr: u8,
    // oamdata: u8,
    // ppuscroll: u8,
    ppuaddr: u16,
    ppuaddr_status: bool,
    pub chr_rom: &'a [u8],
    pub name_tbl_0: [u8; 0x3C0],
    //mirror
    pub back_palette: [u8; 0x0010],
    //mirror
    pub color: [(u8, u8, u8); 64],
}

impl<'a> PPU<'a> {
    const PPUADDR_INC_32: u8 = 0b0000_0010;
    fn ppu_error(&self, address: u16) -> ! {
        eprintln!(
            "accessed ppu undefined or restrected address: 0x{:x}",
            address
        );
        std::process::exit(1)
    }
    fn ppu_warning(&self, address: u16) {
        println!(
            "accessed ppu register is currently not implemented so skip: 0x{:x}",
            address
        );
    }
    pub fn read_ppu(&mut self, address: u16) -> u8 {
        match address {
            0x2007 => self.x2007_read_ppudata(),
            _ => self.ppu_error(address),
        }
    }
    pub fn write_ppu(&mut self, address: u16, data: u8) {
        match address {
            0x2000 => self.ppu_warning(address),
            0x2001 => self.ppu_warning(address),
            0x2005 => self.ppu_warning(address),
            0x2006 => self.x2006_write_ppuaddr(data),
            0x2007 => self.x2007_write_ppudata(data),
            _ => self.ppu_error(address),
        }
    }
    pub fn x2007_read_ppudata(&mut self) -> u8 {
        let res = match self.ppuaddr {
            0x2000..=0x23BF => self.name_tbl_0[(self.ppuaddr - 0x2000) as usize],
            0x3F00..=0x3F0F => self.back_palette[(self.ppuaddr - 0x3F00) as usize],
            _ => self.ppu_error(self.ppuaddr),
        };
        self.inc_ppuaddr();
        res
    }
    pub fn x2006_write_ppuaddr(&mut self, addr: u8) {
        if self.ppuaddr_status {
            self.ppuaddr &= 0b1111_1111_0000_0000;
            self.ppuaddr += addr as u16;
            self.ppuaddr_status = false;
        } else {
            self.ppuaddr &= 0b0000_0000_1111_1111;
            self.ppuaddr += (addr as u16) << 8;
            self.ppuaddr_status = true;
        }
    }
    pub fn x2007_write_ppudata(&mut self, data: u8) {
        match self.ppuaddr {
            0x2000..=0x23BF => self.name_tbl_0[(self.ppuaddr - 0x2000) as usize] = data,
            0x3F00..=0x3F0F => self.back_palette[(self.ppuaddr - 0x3F00) as usize] = data,
            _ => self.ppu_error(self.ppuaddr),
        }
        self.inc_ppuaddr();
    }
    pub fn inc_ppuaddr(&mut self) {
        if (self.ppuctrl & Self::PPUADDR_INC_32) == Self::PPUADDR_INC_32 {
            self.ppuaddr += 0x20;
        } else {
            self.ppuaddr += 0x01;
        }
    }
    pub fn new(chr_rom: &'a [u8]) -> Self {
        PPU {
            ppuctrl: 0,

            ppuaddr: 0,
            ppuaddr_status: false,
            chr_rom,
            name_tbl_0: [0; 0x3C0],
            //mirror
            back_palette: [0; 0x0010],
            //mirror
            color: [
                (84, 84, 84),
                (0, 30, 116),
                (8, 16, 144),
                (48, 0, 136),
                (68, 0, 100),
                (92, 0, 48),
                (84, 4, 0),
                (60, 24, 0),
                (32, 42, 0),
                (8, 58, 0),
                (0, 64, 0),
                (0, 60, 0),
                (0, 50, 60),
                (0, 0, 0),
                (0, 0, 0),
                (0, 0, 0),
                (152, 150, 152),
                (8, 76, 196),
                (48, 50, 236),
                (92, 30, 228),
                (136, 20, 176),
                (160, 20, 100),
                (152, 34, 32),
                (120, 60, 0),
                (84, 90, 0),
                (40, 114, 0),
                (8, 124, 0),
                (0, 118, 40),
                (0, 102, 120),
                (0, 0, 0),
                (0, 0, 0),
                (0, 0, 0),
                (236, 238, 236),
                (76, 154, 236),
                (120, 124, 236),
                (176, 98, 236),
                (228, 84, 236),
                (236, 88, 180),
                (236, 106, 100),
                (212, 136, 32),
                (160, 170, 0),
                (116, 196, 0),
                (76, 208, 32),
                (56, 204, 108),
                (56, 180, 204),
                (60, 60, 60),
                (0, 0, 0),
                (0, 0, 0),
                (236, 238, 236),
                (168, 204, 236),
                (188, 188, 236),
                (212, 178, 236),
                (236, 174, 236),
                (236, 174, 212),
                (236, 180, 176),
                (228, 196, 144),
                (204, 210, 120),
                (180, 222, 120),
                (168, 226, 144),
                (152, 226, 180),
                (160, 214, 228),
                (160, 162, 160),
                (0, 0, 0),
                (0, 0, 0),
            ],
        }
    }
}
