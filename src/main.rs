mod famicom;
use famicom::{cpu::*, memory::*};
use sdl2::pixels::Color;
use std::{env, fs, process, thread, time};

const HEADER_SIZE: usize = 0x0010;
const PRG_ROM_PAGE_SIZE: usize = 0x4000;
const CHR_ROM_PAGE_SIZE: usize = 0x2000;

fn main() {
    let mut args = env::args().into_iter();
    let command_name = args.next().unwrap();
    let rom_data = read_file(&mut args).unwrap_or_else(|(kind, message)| {
        eprintln!("{}: {} {}", kind, command_name, message);
        process::exit(1)
    });
    let sleep_time = args
        .next()
        .unwrap_or(String::from("5"))
        .parse::<u64>()
        .unwrap_or_else(|_| {
            eprintln!("error: sleep time should be unsigned int 64bit");
            process::exit(1)
        });
    let rom_pos = get_rom_position(&rom_data);
    run(rom_pos, sleep_time);
}

fn read_file(args: &mut env::Args) -> Result<Vec<u8>, (&'static str, &'static str)> {
    let rom_name = args.next().ok_or((
        "usage",
        "<rom file> [sleep time per loop(ms: u64(default == 5))]",
    ))?;
    let rom = fs::read(&rom_name).or(Err(("error", "cannot open file")))?;
    Ok(rom)
}

fn get_rom_position<'a>(rom: &'a [u8]) -> RomData<'a> {
    let prg_rom_page_num = rom[4];
    let chr_rom_page_num = rom[5];
    let prg_rom_start: usize = HEADER_SIZE;
    let prg_rom_end: usize = prg_rom_start + PRG_ROM_PAGE_SIZE * prg_rom_page_num as usize;
    let chr_rom_start: usize = prg_rom_end;
    let chr_rom_end: usize = chr_rom_start + CHR_ROM_PAGE_SIZE * chr_rom_page_num as usize;
    println!(
        "PRG_ROM_SIZE: 0x{:x}, CHR_ROM_SIZE: 0x{:x}",
        prg_rom_end - prg_rom_start,
        chr_rom_end - chr_rom_start
    );
    RomData::new(
        &rom[prg_rom_start..prg_rom_end],
        &rom[chr_rom_start..chr_rom_end],
    )
}

fn sdl_init() -> sdl2::render::Canvas<sdl2::video::Window> {
    let mut canvas = sdl2::init()
        .unwrap()
        .video()
        .unwrap()
        .window("Hello, Famicom!", 256, 240)
        .position_centered()
        .build()
        .unwrap()
        .into_canvas()
        .build()
        .unwrap();
    canvas.set_draw_color(Color::RGB(0, 0, 0));
    canvas.clear();
    canvas.present();

    canvas
}

fn run(rom_data: RomData, sleep_time: u64) {
    let mut canvas = sdl_init();

    let mut cpu = CPU::new();
    let mut memory = Memory::new(rom_data);
    cpu.reset(&mut memory);

    for _ in 0..250 {
        cpu.run(&mut memory);

        let screen = get_screen(&memory);

        print(&mut canvas, screen);
        thread::sleep(time::Duration::from_millis(sleep_time));
    }
}

fn get_screen(memory: &Memory) -> [[(u8, u8, u8); 256]; 240] {
    let mut g = [[(0_u8, 0_u8, 0_u8); 256]; 240];

    for i in 0..30 {
        for j in 0..32 {
            let index = 16 * (memory.ppu.name_tbl_0[j + i * 32]) as usize;

            for k in 0..8 {
                let sprite = memory.ppu.chr_rom[index + k];
                let sprite2 = memory.ppu.chr_rom[index + k + 8];

                for h in 0..8 {
                    let bit = 0b1000_0000 >> h;

                    let set = match ((sprite & bit) == bit, (sprite2 & bit) == bit) {
                        (true, true) => memory.ppu.back_palette[3],
                        (true, _) => memory.ppu.back_palette[1],
                        (_, true) => memory.ppu.back_palette[2],
                        _ => memory.ppu.back_palette[0],
                    };
                    g[i * 8 + k][j * 8 + h] = memory.ppu.color[set as usize];
                }
            }
        }
    }

    g
}

fn print(
    canvas: &mut sdl2::render::Canvas<sdl2::video::Window>,
    screen: [[(u8, u8, u8); 256]; 240],
) {
    for i in 0..240 {
        for j in 0..256 {
            canvas.set_draw_color(Color::RGB(screen[i][j].0, screen[i][j].1, screen[i][j].2));
            canvas
                .draw_point(sdl2::rect::Point::new(j as i32, i as i32))
                .unwrap();
        }
    }
    canvas.present();
}
