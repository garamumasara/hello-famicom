pub mod cpu;
pub mod memory;
mod ppu;

const SHIFT_BYTE: u8 = 8;

fn gen_word(low: u8, high: u8) -> u16 {
    ((high as u16) << SHIFT_BYTE) + low as u16
}
