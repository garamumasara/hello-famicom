# Hello, World! Instruction Set

## ROM URL

[http://hp.vector.co.jp/authors/VA042397/nes/sample.html](http://hp.vector.co.jp/authors/VA042397/nes/sample.html)

## Set Interrupt Disable Status

```asm
0x78, sei  
I |= 1
```

addressing|assembler|opc|bytes|cyles
-|-|-|-|-
implied|`SEI`|78|1|2

## Load Index X with Memory

### immediate

```asm
0xA2, ldx #$BB
X = Mem, N+, Z+
```

addressing|assembler|opc|bytes|cyles
-|-|-|-|-
immidiate|`LDX #oper`|A2|2|2

## Transfer Index X to Stack Register

```asm
0x9A, txs
SP = X
```

addressing|assembler|opc|bytes|cyles
-|-|-|-|-
implied|`TXS`|9A|1|2

## Load Accumulator with Memory

### immediate

```asm
0xA9, lda #$BB
A = Mem, N+, Z+
```

### absolute, X

```asm
0xBD, lda $LLHH, X
A = [Mem + X], N+, Z+
```

addressing|assembler|opc|bytes|cyles
-|-|-|-|-
immidiate|`LDA #oper`|A9|2|2
absolute,X|`LDA oper,X`|BD|3|4*

## Store Accumulator in Memory

### absolute

```asm
0x8D, sta $LLHH
[Mem] = A
```

addressing|assembler|opc|bytes|cyles
-|-|-|-|-
absolute|`STA oper`|8D|3|4

## Load Index Y with Memory

### immediate

```asm
0xA0, ldy #$BB
Y = Mem, N+, Z+
```

addressing|assembler|opc|bytes|cyles
-|-|-|-|-
immidiate|`LDY #oper`|A0|2|2

## Increment Index X by One

```asm
0xE8, inx
++X, N+, Z+,
```

addressing|assembler|opc|bytes|cyles
-|-|-|-|-
 implied|INX|E8|1|2

## Decrement Index Y by One

```asm
0x88, dey
--Y, N+, Z+
```

addressing|assembler|opc|bytes|cyles
-|-|-|-|-
implied|DEC|88|1|2

## Branch on Result not Zero

### relative

```asm
0xD0, bne $BB
if Z != 0 then PC = PCH + ((PCL + BB)(8bit-Signed))
```

addressing|assembler|opc|bytes|cyles
-|-|-|-|-
relative|BNE oper|D0|2|2**

## Jump to New Location

### absolute

```asm
0x4C, jmp $LLHH
PC = Mem
```

addressing|assembler|opc|bytes|cyles
-|-|-|-|-
absolute|JMP oper|4C|3|3

## Source

[https://www.masswerk.at/6502/6502_instruction_set.html](https://www.masswerk.at/6502/6502_instruction_set.html)
